<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('status_id');
            $table->foreign('status_id')->references('id')->on('order_statuses')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('customer_id');
            $table->foreign('customer_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('code');
            $table->enum('order_type', ['delivery', 'pickup']);
            $table->text('note')->nullable();
            $table->decimal('subtotal_price', 19, 2);
            $table->decimal('total_price', 19, 2);
            $table->decimal('profit', 19, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
