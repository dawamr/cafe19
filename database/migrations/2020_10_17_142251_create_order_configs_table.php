<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_configs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->boolean('accept_online');
            $table->boolean('delivery');
            $table->boolean('pickup');
            $table->boolean('fee');
            $table->text('fee_description');
            $table->integer('fee_sales_tax');
            $table->boolean('fee_delivery_only');
            $table->enum('fee_tax', ['percentage', 'fixed']);
            $table->enum('fee_tax_type', ['add_to_price', 'include_in_price']);
            $table->boolean('allow_anonym');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_configs');
    }
}
