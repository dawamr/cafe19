<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use Carbon\Carbon;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => (String) Str::uuid(), 'name' => 'admin', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => (String) Str::uuid(), 'name' => 'waitress', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        Role::insert($data);
    }
}
